import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ActionSheetController, ToastController, Platform, Loading } from 'ionic-angular';
import { Http,HttpModule, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';

declare var cordova: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
lastImage: string = null;
resume: string = null;
loading: Loading;
public fname = '';
public lname = '';
public email = '';
public phone = '';
public address = '';
public state = '';
public city = '';
public isDone = false;
public profilepicsrc = "http://www.templatesriver.com/signupApp/uploads/"+this.lastImage;
public resumelink = "http://www.templatesriver.com/signupApp/uploads/"+this.resume;

  constructor(public navCtrl: NavController,
              public loadingCtrl:LoadingController,
              public alertCtrl: AlertController,
              public http:Http,
              private camera: Camera, 
              private transfer: Transfer, 
              private file: File, 
              private filePath: FilePath, 
              public actionSheetCtrl: ActionSheetController, 
              public toastCtrl: ToastController, 
              public platform: Platform ) {
  }

  public uploadImage() {
    // Destination URL
    var url = "http://templatesriver.com/signupApp/upload.php";
   
    // File for Upload
    var targetPath = this.pathForImage(this.lastImage);
   
    // File name only
    var filename = this.lastImage;
   
    var options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : {'fileName': filename}
    };
   
    const fileTransfer: TransferObject = this.transfer.create();
   
    this.loading = this.loadingCtrl.create({
      content: 'Uploading...',
    });
    this.loading.present();
   
    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(data => {
      this.loading.dismissAll()
      this.presentToast('Image succesful uploaded.');
    }, err => {
      this.loading.dismissAll()
      this.presentToast('Error while uploading file.');
    });
  }

  public uploadResume(){
      // Destination URL
      var url = "http://templatesriver.com/signupApp/upload.php";
     
      // File for Upload
      var targetPath = this.pathForImage(this.resume);
     
      // File name only
      var filename = this.resume;
     
      var options = {
        fileKey: "file",
        fileName: filename,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params : {'fileName': filename}
      };
     
      const fileTransfer: TransferObject = this.transfer.create();
     
      this.loading = this.loadingCtrl.create({
        content: 'Uploading...',
      });
      this.loading.present();
     
      // Use the FileTransfer to upload the image
      fileTransfer.upload(targetPath, url, options).then(data => {
        this.loading.dismissAll()
        this.presentToast('Resume succesful uploaded.');
      }, err => {
        this.loading.dismissAll()
        this.presentToast('Error while uploading file.');
      });
    
  }

  SuccessMsg() {
    let alert = this.alertCtrl.create({
      title: 'Success',
      subTitle: 'User is Successfully Created.',
      buttons: ['Ok']
    });
    this.isDone = true;
    alert.present();
  }

  public register(){
    let validate = true;
    if(this.fname==''){
      validate = false;
      this.presentToast('Please Fill Your First Name.');
    }
    if(this.lname==''){
      validate = false;
      this.presentToast('Please Fill Your Last Name.');
    }
    if(this.email==''){
      validate = false;
      this.presentToast('Please Fill Your Email.');
    }
    if(this.phone==''){
      validate = false;
      this.presentToast('Please Fill Your Mobile No.');
    }
    if(this.address==''){
      validate = false;
      this.presentToast('Please Fill Your Address.');
    }
    if(this.state==''){
      validate = false;
      this.presentToast('Please Fill Your State.');
    }
    if(this.city==''){
      validate = false;
      this.presentToast('Please Fill Your City.');
    }
    if(validate){
    let loader = this.loadingCtrl.create({content:'Registering...'});
    loader.present();
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
 
    let postParams = {
      fname : this.fname,
      lname: this.lname,
      email: this.email,
      phone: this.phone,
      address: this.address,
      state: this.state,
      city: this.city,
      resume: this.resume,
      profile: this.lastImage
    }
    
    this.http.post("http://templatesriver.com/signupApp/signup.php", JSON.stringify(postParams), options)
      .map(res => res.json())
      .subscribe(data => {
            JSON.stringify(data);
            console.log(data);
            loader.dismiss();
            //  this.fname = '';
            //  this.lname = '';
            //  this.email = '';
            //  this.phone = '';
            //  this.address = '';
            //  this.state = '';
            //  this.city = '';
            //  this.resume = '';
            //  this.lastImage = '';
             this.SuccessMsg();
            console.log("Registered Successfully!");
            this.presentToast('Sign Up Successful.');
       }, error => {
        console.log(error);
        loader.dismiss();
          console.log("Register Process Failed.");
          this.presentToast('Sign Up Failed.');
      });
    }
  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  // for resume action sheet
  public presentResumeActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Resume',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.SelectResume(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
   
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      }else{
        var currentName2 = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath2 = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath2, currentName2, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
  }

  //for resume

  public SelectResume(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
   
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      }else{
        var currentName2 = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath2 = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath2, currentName2, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting File.');
    });
  }

  // Create a new name for the image
private createFileName() {
  var d = new Date(),
  n = d.getTime(),
  newFileName =  n + ".jpg";
  return newFileName;
}
 
  // Create a new Resume name
private createResumeName() {
  var d = new Date(),
  n = d.getTime(),
  newFileName =  n + ".docs";
  return newFileName;
}
// Copy the image to a local folder
private copyFileToLocalDir(namePath, currentName, newFileName) {
  this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
    this.lastImage = newFileName;
  }, error => {
    this.presentToast('Error while storing file.');
  });
}

// Copy the resume to a local folder
private copyResumeToLocalDir(namePath, currentName, newFileName) {
  this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
    this.resume = newFileName;
  }, error => {
    this.presentToast('Error while storing file.');
  });
}
 
private presentToast(text) {
  let toast = this.toastCtrl.create({
    message: text,
    duration: 3000,
    position: 'top'
  });
  toast.present();
}
 
// Always get the accurate path to your apps folder
public pathForImage(img) {
  if (img === null) {
    return '';
  } else {
    return cordova.file.dataDirectory + img;
  }
}

}
