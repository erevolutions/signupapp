<?php
require 'tomb/DB.php';
  if (isset($_SERVER['HTTP_ORIGIN'])) {
     header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
     header('Access-Control-Allow-Credentials: true');
     header('Access-Control-Max-Age: 86400');    // cache for 1 day
   }
  if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
   if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
       header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

   if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
       header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

     exit(0);
   }
    $postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$fname = $request->fname;
	$lname = $request->lname;
	$email = $request->email;
	$phone = $request->phone;
	$address = $request->address;
	$state = $request->state;
	$city = $request->city;
	$resume = $request->resume;
	$profile = $request->profile;
	$response = DB::table('users')->insert(array('fname'=>$fname,'lname'=>$lname,'email'=>$email,'phone'=>$phone,'address'=>$address,'state'=>$state,'city'=>$city,'pic'=>$profile,'resume'=>$resume));
	if($response){
	 $msg = "User Successfully Created!";
	}else{
	 $msg = "Something went wrong.".$response;
	}
	print_r(json_encode($msg));
	